using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortaStrettaOppoEnvironment : MonoBehaviour
{
    public Environment environment;
    public GameObject finalTarget;

    private float rangeXMin = -8f;
    private float rangeXMax = 8f;
   

    // Start is called before the first frame update
    void Start()
    {
        environment.environmentTerminated += shiftEnvObj;

    }

    private void shiftEnvObj(float obj, Environment env)
    {
        //finale target position
        float posX = Random.Range(rangeXMin, rangeXMax);
    
        Vector3 nuovaPosizione = new Vector3(posX, finalTarget.transform.localPosition.y, finalTarget.transform.localPosition.z);
        finalTarget.transform.localPosition = nuovaPosizione;

    }


}
