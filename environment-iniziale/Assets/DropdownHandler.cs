using System.Collections;
using System.Collections.Generic;
using Unity.Barracuda;
using Unity.MLAgents.Policies;
using UnityEngine;
using UnityEngine.UI;

public class DropdownHandler : MonoBehaviour
{
    public bool brainSelect;
    public List<NNModel> Brain;
    public List<Environment> Env;
    public static Environment selectedEnv;
    public Dropdown dropdown;
    public BehaviorParameters parameters;


    private void Awake()
    {
        if (brainSelect)
        {
            foreach (var b in Brain)
            {
                var item = new Dropdown.OptionData(b.name);
                dropdown.options.Add(item);
            }
            parameters.Model = Brain[0];
        }
        if (!brainSelect)
        {
            foreach (var e in Env)
            {
                var item = new Dropdown.OptionData(e.name);
                dropdown.options.Add(item);
            }
            selectedEnv = Env[0];
        }
    }
    public void OnDropdownValueChanged(int index)
    {
        parameters.Model = Brain[index];
        Debug.Log("Hai selezionato l'opzione " + index);
    }
    public void OnDropdownEnvValueChanged(int index)
    {
        selectedEnv = Env[index];
        Debug.Log("Hai selezionato l'opzione " + index);
    }
}
