using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Range(2, 15)] public float Width;
    [Range(2, 15)] public float Height;

    private void Start()
    {
        RLAgent[] agents = transform.GetComponentsInChildren<RLAgent>(true);
        print(agents);
        foreach(var agent in agents)
        {
            agent.resetAgent += SpawnerRandomPositionl;
        }
    }
 
    private void SpawnerRandomPositionl(RLAgent agent) {
        bool fail = true;
        int failSafe = 0;
        Collider[] empty = new Collider[10];
        while (fail && failSafe < 50)
        {
            fail = false;
            float randomW = Random.Range(-Width / 2f, Width / 2f);
            float randomH = Random.Range(-Height / 2f, Height / 2f);
            Vector3 randomPosition = transform.position + new Vector3(randomW, 0f, randomH);
            int hitSomething = Physics.OverlapSphereNonAlloc(randomPosition, 1.5f, empty, 1 << LayerMask.NameToLayer("Agente"));
            if (hitSomething != 0)
            {
                fail = true;
                failSafe++;
            }
            else
            {
                agent.transform.position = randomPosition;
                Quaternion currentRotation = agent.transform.rotation;
                float randomAngle = Random.Range(-30f, 30f);
                Quaternion randomRotation = Quaternion.Euler(0f, randomAngle, 0f);
                Quaternion newRotation = currentRotation * randomRotation;
                agent.transform.rotation = newRotation;

            }


        }
        if (failSafe>= 50)
        {
            Debug.LogError("Failed to inizialize spawner");
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(Width, 1, Height));
    }
}
