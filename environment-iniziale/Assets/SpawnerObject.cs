using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerObject : MonoBehaviour
{
    public Environment environment;
    public GameObject Object;


    [Range(0, 15)] public float Width;
    [Range(0, 15)] public float Height;


    // Start is called before the first frame update
    void Start()
    {
        environment.environmentTerminated += shiftEnvObj;

    }

    private void shiftEnvObj(float obj, Environment env)
    {
        
        float randomW = Random.Range(-Width / 2f, Width / 2f);
        float randomH = Random.Range(-Height / 2f, Height / 2f);

        Vector3 randomPosition = transform.position + new Vector3(randomW, 0f, randomH);
        Object.transform.position = randomPosition;

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, new Vector3(Width, 1, Height));
    }
}
