# Environment iniziale - WayFinding

## Requisiti Necessari
- Unity Version 2020.3.3f1
- Unity Hub 2.4.2
- Python 3.6.8

### Librerie Python Necessarie
```bash
pip install -r /path/to/requirements.txt
```

- [PyTorch](https://pytorch.org/get-started/locally/)
- [TensorFlow](https://www.tensorflow.org/install/pip)

## Installazione Unity
- Installare Unity Hub dal link: https://unity3d.com/get-unity/download
- Dalle impostazioni andare su _License Management_ e attivare la licenza (ad esempio tramite mail universitaria), poi tornare indietro nella schermata principale
- Nella sezione _Installs_ cliccare sul bottone _Add_ e installare la versione richiesta, se questa non si trova cliccare su _download archives_ e cercarla

## Apertura repo
- Clonare la repository (Ad esempio con GitHubDesktop) e scegliere il branch Thomas-Nuove-Osservazioni
- Aprire Unity Hub e nella sezione _Projects_ aggiungere tramite il bottone _Add_ il progetto Unity (Rl Agents Tesi) dalla repository (Percorso dovrebbe essere .../environment-iniziale/Environment-Iniziale/**RL Agents Tesi**)
- Selezionare poi la versione di Unity richiesta
- Aprire il progetto

## Scena iniziale
Nella cartella Scenes selezionare la scena WayFinding. 

Nella scena sono presenti:
- Main camera (Camera per gli ambienti 20x20) utilizzabile in game-mode tramite il Display 1
- Main camera 40x40 (Camera per ambienti più grandi di 20x20) utilizzabile in game-mode tramite il Display 2
- Directional light, per la luce
- TrainFinale, un Curriculum Manager utilizzato per il training finale
- Test, un Curriculum Manager utilizzato per il test finale


Per visibilità:

**Nella cartella Assets -> Layout è presente il file Layout che è possibile caricare da Window -> Layout -> Load Layout from File...**
- Togliere la griglia (quadrato in rosso nell'immagine)  
- Per la visuale: Top senza Perspective (click destro sul sistema di coordinate, come nell'immagine)
- Utilizzare Unity con Scena e Game-mode in due finestre separate (Fase di retrain ad esempio gli ambienti si vedono nella finestra scena e non in game-mode. Per vederli in game mode si può spostare la visuale della camera o creare una camera nuova) -> Vedi immagine 

<div align="center">
<img src="https://cdn.discordapp.com/attachments/598932779002101859/857214256457318420/SchermataIniziale.png"  width="560" height="300">
</div>

## Link Utili:
- [Come creare un ambiente](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Creazione-Ambiente)
- [Utilizzo del Gestore Ambiente](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Utilizzo-del-Gestore-Ambiente)
- [Utilizzo del Gestore Curriculum](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Utilizzo-del-Gestore-Curriculum)
- [Come effettuare un Training](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Come-effettuare-il-Training)
- [Come salvare i dati per analizzarli in Python](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Come-utilizzare-Python)
- [TensorBoard e andamento delle reward](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/TensorBoard-e-andamento-delle-reward)
